<?php

class Home {

	public static function getMetaInfo()
	{
		$arrData = [];
		if( Cache::has('meta_info') ) {
			$arrData = Cache::get('meta_info');
		} else {
			$arrMeta = ['title_site', 'meta_description', 'main_logo', 'favicon'];
			$configures = Configure::select('ckey', 'cvalue')->whereIn('ckey', ['title_site', 'meta_description', 'main_logo', 'favicon'])->get();
			foreach($configures as $configure) {
				$arrData[$configure['ckey']] = $configure['cvalue'];
			}
			foreach($arrMeta as $key) {
				if( !isset($arrData[$key]) ) $arrData[$key] = '';
			}
			Cache::forever('meta_info', $arrData);
		}

		return $arrData;
	}

	public static function getBanner()
	{
		$html = '';
		if( Cache::has('banners') ) {
			$html = Cache::get('banners');
		} else {
			$banners = Banner::select('id', 'name')->where('active', 1)->orderBy('order_no', 'asc')->with('images')->get()->toArray();
			foreach($banners as $banner) {
				$image = '';
				if( !empty($banner['images']) ) {
					$image = reset($banner['images']);
					$image = $image['path'];
				}
				if( empty($image) ) {
					continue;
				}
				$html .= '<img src="'.URL.'/'.$image.'" alt="'.$banner['name'].'" title="'.$banner['name'].'" />';
			}
			Cache::forever('banners', $html);
		}
		return $html;
	}
}