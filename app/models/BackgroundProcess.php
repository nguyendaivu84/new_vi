<?php

class BackgroundProcess {

	public static function resize($width, $path, $name)
	{
		$cmd = 'php '.ARTISAN." image:process --type=resize --path={$path} --name={$name} --width={$width}";
		return self::proccess($cmd);
	}

	public static function makeThumb($path, $name)
	{
		$cmd = 'php '.ARTISAN." image:process --type=thumb --path={$path} --name={$name}";
		return self::proccess($cmd);
	}

	public static function copyFromVI()
	{
		$cmd = 'php '.ARTISAN." image:process --type=copy-vi";
		return self::proccess($cmd);
	}

	public static function sync()
	{
		$cmd = 'php '.ARTISAN." getDB:get";
		return self::proccess($cmd);
	}

	public static function proccess($cmd)
	{
		if( DS == '\\') {
	        	return pclose(popen("start /B ". $cmd, "r"));
	   	 } else {
	       		return exec($cmd.' > /dev/null &');
	    	}
	}

}
