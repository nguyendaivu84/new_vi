<?php

class UserController extends BaseController {

	public function login()
	{
		$this->layout->content = View::make('frontend.login');
	}

	public function signup()
	{
		$this->layout->content =  View::make('frontend.signup');
	}

	public function create()
	{
		$user = new User;
		$user->email = Input::has("email")?Input::get("email"):'';
		if( Input::has('password') ) {
			$password = Input::get('password');
			if( !empty($password) ) {
				$user->password = $password;
				$user->password_confirmation = Input::get('password_confirmation');
			}
		}
		$user->active = 1;
		$pass = $user->valid();
		if($pass->passes()) {
			if( isset($user->password_confirmation) ) {
				unset($user->password_confirmation);
			}
			$user->password = Hash::make($user->password);
			$user->save();
			Auth::user()->loginUsingId($user->id);
			return Redirect::to('/user/addresses');
		}
		return Redirect::to('/user/signup')->with('error',$pass->messages()->all())->withInput();
	}

	public function logout()
	{
		Auth::user()->logout();
		return Redirect::to('/');
	}

	public function addresses()
	{
		$countries = JTCountry::getSource();
		$this->layout->content = View::make('frontend.address')->with([
											  'addresses'  => Address::where('user_id',"=",Auth::user()->get()->id)
																		->orderBy('default','dsc')
																		->get(),
											'countries'	=> $countries
											]);
	}

	public function updateAddress()
	{
		$arr_return['status'] = 'ok';
		$id = Input::has('id')?Input::get('id'):0;
		if($id) {
			$address = Address::find($id);
		} else {
			$address = new Address;
		}

		$address->first_name = Input::has('first_name')?Input::get('first_name'):'';
		$address->last_name = Input::has('last_name')?Input::get('last_name'):'';
		$address->company = Input::has('company')?Input::get('company'):'';
		$address->address1 = Input::has('address1')?Input::get('address1'):'';
		$address->address2 = Input::has('address2')?Input::get('address2'):'';
		$address->city = Input::has('city')?Input::get('city'):'';
		$address->country_id = Input::has('country')?Input::get('country'):0;
		$address->province_id = Input::has('province')?Input::get('province'):0;
		$address->zipcode = Input::has('zipcode')?Input::get('zipcode'):'';
		$address->phone = Input::has('phone')?Input::get('phone'):'';

		if(Input::has('default')) {
			$address->default = Input::get('default')=="true"?1:0;
		} else {
			$address->default = 0;
		}
		if($id==0) {
			  $address->user_id = Auth::user()->get()->id;
			  $address->type = 'User';
		}
		$check = $address->save();
		if($check) {
			$arr_return['status'] = 'ok';
		}
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	public function deleteAddress()
	{
		$user_id = Auth::user()->get()->id;
		$id = Input::has('id')?Input::get('id'):0;
		$address = Address::find($id);
		if($address) {
			if($address->user_id == $user_id) {
				if($address->delete()) {
					$arr_return['status']='ok';
				} else {
					$arr_return['status']='error';
					$arr_return['message']='Error delete address';
				}
			} else {
				$arr_return['status']='error';
				$arr_return['message']='You do not have permission to delete this address';
			}
		} else {
			$arr_return['status']='error';
			$arr_return['message']='Can not find address';
		}

		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}



	public function forgotPassword()
	{
		$response = Password::user()->remind(Input::only('email'), function($message) {
									  $message->subject('Password reminder');
								  });
		switch ($response ) {
			case Password::INVALID_USER:
					  return Redirect::to('/user/login')
								->with('error', 'Not found email.')
								->with('forgot',true);

			case Password::REMINDER_SENT:
					  return Redirect::to('/user/login')
								->with('error', 'We sent you an email to reset your password.')
								->with('forgot',true);
		}

	}

	public function resetPassword($token)
	{
		$this->layout->content =  View::make('frontend.reset_password')->with('token',$token);
	}

	public function updatePassword()
	{
		$credentials = array(
							  'email' => Input::get('email'),
							  'password' => Input::get('password'),
							  'password_confirmation' => Input::get('password_confirmation'),
							  'token' => Input::get('token')
						  );

		$response = Password::user()->reset($credentials, function($user, $password) {
			  $user->password = Hash::make($password);
			  $user->save();

		 });
		switch ($response) {
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
				return Redirect::to('/password/reset/'.Input::get('token'))
											->with('error', Lang::get($response))->withInput();

			case Password::PASSWORD_RESET:
			 	return Redirect::to('/user/login')
										->with('error', 'Your password has been reset.')->withInput();
		}
	}

}