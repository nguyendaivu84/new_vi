<?php

class HomeController extends BaseController {

	public function index()
	{
		$this->layout->content = View::make('frontend.index')->with([
												'banner' => Home::getBanner()
											]);
	}

	public function getVIImages()
	{
		if( Request::ajax() ){
			$tags = Input::has('tags') ? Input::get('tags') : '';
			$image = VIImage::getOthers(['tags' => $tags]);
			$arrReturn = [];
			if( $image['total'] ) {
				try {
					$service = GoogleDrive::connect();
				} catch(Exception $e) {
					$service = false;
				}
				foreach($image['images'] as $image) {
					if( $image['store'] == 'google-drive' && $image['file_id'] ) {
						if( !$service ) {
							continue;
						}
						$key = md5('150x150jpg');
						$file = GoogleDrive::getFile($image['file_id'], $service);
						if( Cache::tags(['images', $image['id']])->has($key) ) {
							$thumb = URL.'/thumb/'.$image['id'].'/600x450.jpg';
						} else {
							$thumb = URL.'/thumb/'.$image['id'].'/600x450.jpg?path='.urlencode($file->thumbnailLink);
						}
						$ext = $file->mimeType;
						$link = $file->downloadUrl;
					} else {
						$thumb = URL.'/thumb/'.$image['id'].'/150x150.jpg';
						$link = URL.'/'.$image['path'];
						$ext = 'image/'.substr($image['path'], strrpos($image['path'], '.') + 1);
					}
					$arrReturn[] = [
									'id' => $image['id'],
									'thumb' => $thumb,
									'link'	=> $link,
									'ext'	=> $ext,
									'store' => $image['store'],
								];
				}
			}
			return $arrReturn;
		}
		return App::abort(404);
	}

	public function saveSessionImgs(){
		$arr_img = array();
		if( Request::ajax() && Input::has('arrImgs')){
			$arrImgs = Input::get('arrImgs');
			$user_ip = Request::getClientIp(true);
	        Session::set('user_ip', $user_ip);
	        $arr_img = Session::has('user_images')?Session::get('user_images'):array();
	        foreach ($arrImgs as $value) {
	        	$arr_img[$user_ip][] = str_replace(URL, "", $value);
	        }
	        Session::set('user_images', $arr_img);
	     }
	     return $arr_img;

	}

	public function deleteSessionImgs(){
		if( !Request::ajax() ) {
			return App::abort(404);
		}
		$arrReturn 	= ['status' => 'error'];
		$src 		= Input::has('src') ? Input::get('src') : '';
		if( !empty($src) ) {
			$userIp = Request::getClientIp(true);
			if( $src == 'all' ) {
				$arrImgs = [$userIp => []];
			} else {
				$src = str_replace(URL.'/', '', $src);
				$arrImgs = Session::has('user_images')?Session::get('user_images'):array();
				foreach ($arrImgs[$userIp] as $key => $value) {
					if( strpos($value, $src) !== false ) continue;
					unset($arrImgs[$userIp][$key]);
				}
			}
	        Session::set('user_images', $arrImgs);
			$arrReturn 	= ['status' => 'ok'];
		}

		return $arrReturn;
	}
}
