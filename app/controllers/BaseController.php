<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
    protected $layout = 'frontend.layout.default';

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
            $this->layout->cartQuantity = CartController::getCartQuantity();
            $this->layout->metaInfo = Home::getMetaInfo();
            $this->layout->headerMenu = Menu::getCache(['header' => true]);
            $this->layout->footerMenu = Menu::getCache(['footer' => true]);
		}
	}

	public static function errors($code = 404, $title = '', $message = '')
    {
        $ajax = Request::ajax();
        if( !$code ) {
            $code = 500;
            $title = 'Internal Server Error';
            $message = 'We got problems over here. Please try again later!';
        } else if( $code == 404 ) {
            $title = 'Oops! You\'re lost.';
            $message = 'We can not find the page you\'re looking for.';
        }
        if( Request::ajax() ) {
            return Response::json([
                'error' => [
                    'message' => $message
                    ]
            ],$code);
        }
        $arrData = [];
        $arrData['cartQuantity'] = CartController::getCartQuantity();
        $arrData['content'] = View::make('frontend.errors.error')->with(['title' => $title, 'code' => $code, 'message' => $message]);
        $arrData['metaInfo'] = Home::getMetaInfo();
        $arrData['metaInfo']['meta_title'] = $title;
        $arrData['headerMenu'] = Menu::getCache(['header' => true]);
        return View::make('frontend.layout.default')->with($arrData);
    }

}
